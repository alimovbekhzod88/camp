<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
        "deluxe/css/open-iconic-bootstrap.min.css",
        "deluxe/css/animate.css",
        "deluxe/css/owl.carousel.min.css",
        "deluxe/css/owl.theme.default.min.css",
        "deluxe/css/magnific-popup.css",

        "deluxe/css/aos.css",

        "deluxe/css/ionicons.min.css",

        "deluxe/css/bootstrap-datepicker.css",
        "deluxe/css/jquery.timepicker.css",


        "deluxe/css/flaticon.css",
        "deluxe/css/icomoon.css",
        "deluxe/css/style.css",
    ];
    public $js = [
        "deluxe/js/jquery.min.js",
        "deluxe/js/jquery-migrate-3.0.1.min.js",
        "deluxe/js/popper.min.js",
        "deluxe/js/bootstrap.min.js",
        "deluxe/js/jquery.easing.1.3.js",
        "deluxe/js/jquery.waypoints.min.js",
        "deluxe/js/jquery.stellar.min.js",
        "deluxe/js/owl.carousel.min.js",
        "deluxe/js/jquery.magnific-popup.min.js",
        "deluxe/js/aos.js",
        "deluxe/js/jquery.animateNumber.min.js",
        "deluxe/js/bootstrap-datepicker.js",
        "deluxe/js/jquery.timepicker.min.js",
        "deluxe/js/scrollax.min.js",
        "deluxe/js/google-map.js",
        "deluxe/js/main.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
