<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parking".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $text
 */
class Parking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'parking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'image', 'text'], 'required'],
            [['text','name','title'], 'string'],
            [['image','name'], 'string', 'max' => 255],
             [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'name' => 'Name',
            'image' => 'Image',
            'text' => 'Text',
        ];
    }
}
