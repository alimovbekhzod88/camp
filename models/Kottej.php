<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kottej".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $text
 */
class Kottej extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'kottej';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image', 'text'], 'required'],
            [['text'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'text' => 'Text',
        ];
    }
}
