<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property int $id
 * @property string $parking_image
 * @property string $kottej_image
 * @property string $blog_image
 * @property string $video
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $parking;
    public $kottej;
    public $blog;
    public $vid;
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parking_image', 'kottej_image', 'blog_image', 'video'], 'required'],
            [['parking_image', 'kottej_image', 'blog_image', 'video'], 'string', 'max' => 255],
            [['parking'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
            [['kottej'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
            [['blog'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
            [['vid'], 'file', 'skipOnEmpty' => true, 'extensions' => 'avi, mp4,mpeg2']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parking_image' => 'Parking Image',
            'kottej_image' => 'Kottej Image',
            'blog_image' => 'Blog Image',
            'video' => 'Video',
        ];
    }
}
