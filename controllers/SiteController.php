<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Meal;
use app\models\Blog;
use app\models\Restaurant;
use app\models\Room;
use app\models\Gallery;
use app\models\Video;
use app\models\Parking;
use yii\helpers\Url;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $video = Video::find()->limit(1)->one();
        return $this->render(
            'about',
            [
                'video' => $video
            ]
        );
    }

    public function actionRestaurant()
    {
        $image = Restaurant::find()->limit(1)->one();
        $meals = Meal::find()->all();
        $mms = Meal::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('restaurant', ['meals' => $meals, 'mms' => $mms, 'image' => $image]);
    }
    public function actionBlog()
    {
        $blogs = Blog::find()->all();
        return $this->render('blog', ['blogs' => $blogs]);
    }

    public function actionBlogSingle()
    {
        return $this->render('blog-single');
    }

    public function actionRoomSingle()
    {
        // $rooms = Room::find()->all();
        return $this->render('room-single');
    }

    public function actionXizmat()
    {
        $park = Parking::find()->limit(1)->one();
        $second = Parking::find()->orderBy(['id' => SORT_DESC])->one();
        // print_r($second);exit;
        return $this->render(
            'Xizmat',
            [
                'park' => $park,
                'second' => $second
            ]
        );
    }
    public function actionRoom()
    {
        $rooms = Room::find()->all();
        return $this->render(
            'room',
            [
                'rooms' => $rooms
            ]
        );
    }
    public function actionFoto()
    {
        $gallery = Gallery::find()->all();
        return $this->render(
            'gallery',
            [
                'gallery' => $gallery
            ]
        );
    }

    public function actionRus()
    {
        Yii::$app->language = 'ru';
        return $this->render("index", [
            'language' => Yii::$app->language
        ]);
        
    }
}
