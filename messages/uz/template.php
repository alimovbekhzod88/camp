<?php
return [
    // 'Bosh_sahifa' => 'Главная',
    'Tibbiyot markazi' => 'медицинский центр',
    'Vip kottej' => 'Вип коттедж',
    'Lyuks kottej' => 'Люкс коттедж',
    'Kottejlar' => 'коттеджи',
    'Lyuks xona' => 'Люкс комната',
    'Pol-Lyuks xona' => 'Пол-люкс комната',
    'Xizmatlar' => 'Услуги',
    'Bog`lanish' => 'Контакт',
    'Besedka' => 'Беседка',
    'Sauna' => 'Сауна',
    'Oshxona' => 'Кухня',
    'Basseyn' => 'Бассейн',
    'Raqs maydoni' => 'Танцевальная зона',
    'Bolalar maydonchasi' => 'Детская площадка',
    'Jonli burchak' => 'Живой уголок',
];
