<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Blog;
use app\models\search\BlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blog();

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'img');
            $vid = UploadedFile::getInstance($model, 'vid');
            if (!empty($img)&&!empty($vid)) {
                $model->image = random_int(0,9999). '.' . $img->extension;
                $model->video = random_int(0,9999). '.' . $vid->extension;
            }elseif (!empty($img)) {
                $model->image = random_int(0,9999). '.' . $img->extension;
                
            }else{
                $model->video = random_int(0,9999). '.' . $vid->extension;
            }
            
            if ($model->save()) {
                if (!empty($img)&&!empty($vid)) {
                    $img->saveAs('uploads/blog/' . $model->image);
                    $vid->saveAs('uploads/blog/' . $model->video);
                    return $this->redirect(['index']);
                }elseif (!empty($img)) {
                    $img->saveAs('uploads/blog/' . $model->image);
                    return $this->redirect(['index']);
                }
                else{
                    $vid->saveAs('uploads/blog/' . $model->video);
                    return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

         if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'img');
            $vid = UploadedFile::getInstance($model, 'vid');
            if (!empty($img)&&!empty($vid)) {
                $model->image = random_int(0,9999). '.' . $img->extension;
                $model->video = random_int(0,9999). '.' . $vid->extension;
            }
            
            if ($model->save()) {
                if (!empty($img)&&!empty($vid)) {
                    $img->saveAs('uploads/blog/' . $model->image);
                    $vid->saveAs('uploads/blog/' . $model->video);
                    return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = Blog::findOne($id);
       unlink(Yii::$app->basePath . '/public_html/uploads/blog/' . $data->image);
       unlink(Yii::$app->basePath . '/public_html/uploads/blog/' . $data->video);
       $this->findModel($id)->delete();

       return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
