<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Gallery;
use app\models\search\GallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile; 

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        if ($model->load(Yii::$app->request->post())) {
            $parking = UploadedFile::getInstance($model, 'parking');
            $kottej = UploadedFile::getInstance($model, 'kottej');
            $blog = UploadedFile::getInstance($model, 'blog');
            $vid = UploadedFile::getInstance($model, 'vid');
            if (!empty($parking)&&!empty($vid)&&!empty($blog)&&!empty($kottej)) {
                $model->parking_image = random_int(0,9999). '.' . $parking->extension;
                $model->kottej_image = random_int(0,9999). '.' . $kottej->extension;
                $model->blog_image = random_int(0,9999). '.' . $blog->extension;
                $model->video = random_int(0,9999). '.' . $vid->extension;
            }
            // elseif (!empty($img)) {
            //     $model->image = random_int(0,9999). '.' . $img->extension;
                
            // }else{
            //     $model->video = random_int(0,9999). '.' . $vid->extension;
            // }
            
            if ($model->save()) {
                if (!empty($parking)&&!empty($vid)&&!empty($blog)&&!empty($kottej)) {
                    $parking->saveAs('uploads/gallery/' . $model->parking_image);
                    $kottej->saveAs('uploads/gallery/' . $model->kottej_image);
                    $blog->saveAs('uploads/gallery/' . $model->blog_image);
                    $vid->saveAs('uploads/gallery/' . $model->video);
                    return $this->redirect(['index']);
                }
                // elseif (!empty($img)) {
                //     $img->saveAs('uploads/gallery/' . $model->image);
                //     return $this->redirect(['index']);
                // }
                // else{
                //     $vid->saveAs('uploads/gallery/' . $model->video);
                //     return $this->redirect(['index']);
                // }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $parking = UploadedFile::getInstance($model, 'parking');
            $kottej = UploadedFile::getInstance($model, 'kottej');
            $blog = UploadedFile::getInstance($model, 'blog');
            $vid = UploadedFile::getInstance($model, 'vid');
            if (!empty($parking)&&!empty($vid)&&!empty($blog)&&!empty($kottej)) {
                $model->parking_image = random_int(0,9999). '.' . $parking->extension;
                $model->kottej_image = random_int(0,9999). '.' . $kottej->extension;
                $model->blog_image = random_int(0,9999). '.' . $blog->extension;
                $model->video = random_int(0,9999). '.' . $vid->extension;
            }
            
            if ($model->save()) {
                if (!empty($parking)&&!empty($vid)&&!empty($blog)&&!empty($kottej)) {
                    $parking->saveAs('uploads/gallery/' . $model->parking_image);
                    $kottej->saveAs('uploads/gallery/' . $model->kottej_image);
                    $blog->saveAs('uploads/gallery/' . $model->blog_image);
                    $vid->saveAs('uploads/gallery/' . $model->video);
                    return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = Gallery::findOne($id);
       unlink(Yii::$app->basePath . '/public_html/uploads/gallery/' . $data->parking_image);
       unlink(Yii::$app->basePath . '/public_html/uploads/gallery/' . $data->kottej_image);
       unlink(Yii::$app->basePath . '/public_html/uploads/gallery/' . $data->blog_image);
       unlink(Yii::$app->basePath . '/public_html/uploads/gallery/' . $data->video);
       $this->findModel($id)->delete();

       return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
