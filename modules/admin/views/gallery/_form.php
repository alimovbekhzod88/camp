<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parking')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kottej')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blog')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vid')->fileInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
