<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej */

$this->title = 'Create Kottej';
$this->params['breadcrumbs'][] = ['label' => 'Kottejs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
