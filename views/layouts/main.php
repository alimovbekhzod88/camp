<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
  <?php $this->beginBody() ?>

  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="<?= Url::to('index') ?>">Deluxe</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">

        <nav>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="<?= Url::to(['site/index']) ?>" class="nav-link"><?= Yii::t("template", 'Bosh_sahifa') ?></a></li>
            <li class="nav-item"><a href="<?= Url::to(['site/index']) ?>" class="nav-link"><?= Yii::t("template",'Tibbiyot_Markazi') ?></a></li>
            <li class="nav-item"><a href="<?= Url::to(['site/room']) ?>" class="nav-link"><?= Yii::t("template","Vip-Kottej")?></a></li>
            <li class="nav-item"><a href="<?= Url::to(['site/room']) ?>" class="nav-link"><?= Yii::t("template","Vip-Kottej")?>Lyuks-Kottej</a></li>
            <li class="nav-item"><a href="<?= Url::to(['site/about']) ?>"></a>
              <a title="Kottejlar" href="#" data-toggle="dropdown" class="nav-link dropdown-toggle" aria-expanded="true">Kottejlar <span class="caret"></span></a>
              <ul role="menu" class=" dropdown-menu">
                <li class="nav-item">
                  <a href="#" class="nav-link">Lyuks xona</a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">Pol-lyuks xona</a>
                </li>
              </ul>
            </li>

            <li class="nav-item"><a href="<?= Url::to(['site/xizmat']); ?>" class="nav-link">Xizmatlar</a></li>
            <li class="nav-item"><a href="<?= Url::to(['site/contact']) ?>" class="nav-link">Bog`lanish</a></li>
          </ul>
        </nav>

        <nav class="navbar-left" style="margin-left: 120px;">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="#" class="nav-link facebook">
                <i class="fa fa-facebook-square"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fa fa-telegram"></i>
              </a>
            </li>
          </ul>
        </nav>

        <nav class="navbar-left">

          <ul class="navbar-nav ml-auto">
            <li class="nav-item nav-li">
              <a href="<?= /*Url::base(true) . '/rus';*/ Url::to(['rus']); ?>" class="nav-language">RUS</a>
              |
              <a href="<?= Url::base(true) ?>" class="nav-language">UZB</a>
            </li>
          </ul>

        </nav>
      </div>
  </nav>

  </div>


  <!-- END nav -->




  <?= $content ?>


  <footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md">
          <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Deluxe Hotel</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
              <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
              <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
              <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>
        </div>
        <div class="col-md">
          <div class="ftco-footer-widget mb-4 ml-md-5">
            <h2 class="ftco-heading-2">Useful Links</h2>
            <ul class="list-unstyled">
              <li><a href="#" class="py-2 d-block">Blog</a></li>
              <li><a href="#" class="py-2 d-block">Rooms</a></li>
              <li><a href="#" class="py-2 d-block">Amenities</a></li>
              <li><a href="#" class="py-2 d-block">Gift Card</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md">
          <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Privacy</h2>
            <ul class="list-unstyled">
              <li><a href="#" class="py-2 d-block">Career</a></li>
              <li><a href="#" class="py-2 d-block">About Us</a></li>
              <li><a href="#" class="py-2 d-block">Contact Us</a></li>
              <li><a href="#" class="py-2 d-block">Services</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md">
          <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Have a Questions?</h2>
            <div class="block-23 mb-3">
              <ul>
                <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">

          <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>
              document.write(new Date().getFullYear());
            </script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </p>
        </div>
      </div>
    </div>
  </footer>



  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>