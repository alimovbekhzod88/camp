<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Restaurants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hero-wrap" style="background-image: url('<?=Yii::getAlias('@web')?>/deluxe/images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?=Url::to(['site/index'])?>">Home</a></span> <span>Restaurants</span></p>
            <h1 class="mb-4 bread">Restaurants</h1>
        </div>
    </div>
</div>
</div>
</div>
<section class="ftco-section ftc-no-pb ftc-no-pt">
    <div class="container">
        <div class="row">
            <div class="col-md-5 p-md-5 img img-2 img-3 d-flex justify-content-center align-items-center" style="background-image: url(<?=Yii::getAlias('@web')?>/uploads/restaurant/<?=$image->image?>);">
            </div>
            <div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
              <div class="heading-section heading-section-wo-line pt-md-4 mb-5">
                <div class="ml-md-0">
                    <span class="subheading">Our Restaurants</span>
                    <h2 class="mb-4"><?=$image->name?></h2>
                </div>
            </div>
            <div class="pb-md-4">
                <p><?=$image->text?></p>
            </div>
        </div>
    </div>
</div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section text-center ftco-animate">
            <h2>Our Menu</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php foreach ($meals as $meal) :?>
            <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/uploads/meal/<?=$meal->image?>);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span><?=$meal->name?></span></h3>
                        <span class="price">$20.00</span>
                    </div>
                    <div class="d-block">
                        <p><?=$meal->text?></p>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
            <!-- <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/menu-2.jpg);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span>Grilled Beef with potatoes</span></h3>
                        <span class="price">$29.00</span>
                    </div>
                    <div class="d-block">
                        <p>A small river named Duden flows by their place and supplies</p>
                    </div>
                </div>
            </div>
            <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/menu-3.jpg);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span>Grilled Beef with potatoes</span></h3>
                        <span class="price">$20.00</span>
                    </div>
                    <div class="d-block">
                        <p>A small river named Duden flows by their place and supplies</p>
                    </div>
                </div>
            </div>
            <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/menu-4.jpg);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span>Grilled Beef with potatoes</span></h3>
                        <span class="price">$20.00</span>
                    </div>
                    <div class="d-block">
                        <p>A small river named Duden flows by their place and supplies</p>
                    </div>
                </div>
            </div> -->
        </div>

        <div class="col-md-6">
            <?php foreach ($mms as $mss) :?>
            <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/uploads/meal/<?=$mss->image?>);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span><?=$mss->name?></span></h3>
                        <span class="price">$49.91</span>
                    </div>
                    <div class="d-block">
                        <p><?=$mss->text?></p>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <!-- <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/menu-6.jpg);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span>Ultimate Overload</span></h3>
                        <span class="price">$20.00</span>
                    </div>
                    <div class="d-block">
                        <p>A small river named Duden flows by their place and supplies</p>
                    </div>
                </div>
            </div>
            <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/menu-7.jpg);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span>Grilled Beef with potatoes</span></h3>
                        <span class="price">$20.00</span>
                    </div>
                    <div class="d-block">
                        <p>A small river named Duden flows by their place and supplies</p>
                    </div>
                </div>
            </div>
            <div class="pricing-entry d-flex ftco-animate">
                <div class="img" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/menu-8.jpg);"></div>
                <div class="desc pl-3">
                    <div class="d-flex text align-items-center">
                        <h3><span>Ham &amp; Pineapple</span></h3>
                        <span class="price">$20.00</span>
                    </div>
                    <div class="d-block">
                        <p>A small river named Duden flows by their place and supplies</p>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
</section>

<section class="instagram pt-5">
  <div class="container-fluid">
    <div class="row no-gutters justify-content-center pb-5">
      <div class="col-md-7 text-center heading-section ftco-animate">
        <h2><span>Instagram</span></h2>
    </div>
</div>
<div class="row no-gutters">
  <div class="col-sm-12 col-md ftco-animate">
    <a href="images/insta-1.jpg" class="insta-img image-popup" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/insta-1.jpg);">
      <div class="icon d-flex justify-content-center">
        <span class="icon-instagram align-self-center"></span>
    </div>
</a>
</div>
<div class="col-sm-12 col-md ftco-animate">
    <a href="images/insta-2.jpg" class="insta-img image-popup" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/insta-2.jpg);">
      <div class="icon d-flex justify-content-center">
        <span class="icon-instagram align-self-center"></span>
    </div>
</a>
</div>
<div class="col-sm-12 col-md ftco-animate">
    <a href="images/insta-3.jpg" class="insta-img image-popup" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/insta-3.jpg);">
      <div class="icon d-flex justify-content-center">
        <span class="icon-instagram align-self-center"></span>
    </div>
</a>
</div>
<div class="col-sm-12 col-md ftco-animate">
    <a href="images/insta-4.jpg" class="insta-img image-popup" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/insta-4.jpg);">
      <div class="icon d-flex justify-content-center">
        <span class="icon-instagram align-self-center"></span>
    </div>
</a>
</div>
<div class="col-sm-12 col-md ftco-animate">
    <a href="images/insta-5.jpg" class="insta-img image-popup" style="background-image: url(<?=Yii::getAlias('@web')?>/deluxe/images/insta-5.jpg);">
      <div class="icon d-flex justify-content-center">
        <span class="icon-instagram align-self-center"></span>
    </div>
</a>
</div>
</div>
</div>
</section>