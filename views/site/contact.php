<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="hero-wrap" style="background-image: url('<?=Yii::getAlias('@web')?>/deluxe/images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
          <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
            <div class="text">
              <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?=Url::to(['site/index'])?>">Home</a></span> <span>Contact</span></p>
              <h1 class="mb-4 bread">Contact Us</h1>
            </div>
          </div>
        </div>
      </div>
    </div>


    <section class="ftco-section contact-section bg-light">
      <div class="container">
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h3">Contact Information</h2>
          </div>
          <div class="w-100"></div>
          <div class="col-md-3 d-flex">
            <div class="info bg-white p-4">
              <p><span>Address:</span> 198 West 21th Street, Suite 721 New York NY 10016</p>
            </div>
          </div>
          <div class="col-md-3 d-flex">
            <div class="info bg-white p-4">
              <p><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
            </div>
          </div>
          <div class="col-md-3 d-flex">
            <div class="info bg-white p-4">
              <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
            </div>
          </div>
          <div class="col-md-3 d-flex">
            <div class="info bg-white p-4">
              <p><span>Website</span> <a href="#">yoursite.com</a></p>
            </div>
          </div>
        </div>
        <div class="row block-9">
          <div class="col-md-6 order-md-last d-flex">
            <?php $form = ActiveForm::begin(['id' => 'contact-form','class'=>'bg-white p-5 contact-form']); ?>
              <div class="form-group">
                <?= $form->field($model, 'name')->textInput(['autofocus' => true,'placeholder'=>'Your Name']) ?>
              </div>
              <div class="form-group">
                <?= $form->field($model, 'email')->textInput(['autofocus' => true,'placeholder'=>'Your Email']) ?>
              </div>
              <div class="form-group">
                <?= $form->field($model, 'subject')->textInput(['autofocus' => true,'placeholder'=>'Subject']) ?>
              </div>
              <div class="form-group">
                <?= $form->field($model, 'body')->textarea(['rows' => 7,'cols'=>30,'placeholder'=>'Message']) ?>
              </div>
              <div class="form-group">
                <?= Html::submitButton('Submit Form', ['class' => 'btn btn-primary py-3 px-5', 'name' => 'contact-button']) ?>
              </div>
            <?php ActiveForm::end(); ?>          
          </div>

          <div class="col-md-6 d-flex">
            <div id="map" class="bg-white"></div>
          </div>
        </div>
      </div>
    </section>
